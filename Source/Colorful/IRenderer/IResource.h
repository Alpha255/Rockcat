#pragma once

#include "Colorful/IRenderer/Format.h"
#include <d3d11.h>

NAMESPACE_START(RHI)

enum ELimitations : uint8_t
{
	MaxRenderTargets = D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT,
	MaxViewports = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE,
	MaxScissorRects = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE,
	MaxSamplers = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT,
	MaxUniformBuffers = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT,
	MaxTextures = D3D11_COMMONSHADER_INPUT_RESOURCE_REGISTER_COUNT,
	MaxRWBufferss = D3D11_1_UAV_SLOT_COUNT,
	MaxVertexStreams = D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT
};

enum class EIndexFormat : uint8_t
{
	UInt16 = sizeof(uint16_t),
	UInt32 = sizeof(uint32_t)
};

enum class EResourceType : uint8_t
{
	Unknown,
	Sampler,
	CombinedImageSampler,
	SampledImage,
	StorageImage,
	UniformTexelBuffer,
	StorageTexelBuffer,
	UniformBuffer,
	StorageBuffer,
	UniformBufferDynamic,
	StorageBufferDynamic,
	InputAttachment,
	PushConstants
};

enum class EDeviceAccessFlags : uint8_t
{
	None,
	GpuRead = 1 << 0,
	GpuReadWrite = 1 << 1,
	CpuRead = 1 << 2,
	CpuWrite = 1 << 3,
	GpuReadCpuWrite = GpuRead | CpuWrite
};
ENUM_FLAG_OPERATORS(EDeviceAccessFlags)

enum class EBufferUsageFlags : uint16_t
{
	None,
	VertexBuffer = 1 << 0,
	IndexBuffer = 1 << 1,
	UniformBuffer = 1 << 2,
	ShaderResource = 1 << 3,
	StreamOutput = 1 << 4,
	RenderTarget = 1 << 5,
	DepthStencil = 1 << 6,
	UnorderedAccess = 1 << 7,
	IndirectBuffer = 1 << 8,
	InputAttachment = 1 << 9,
	ByteAddressBuffer = 1 << 10,
	StructuredBuffer = 1 << 12,
	AccelerationStructure = 1 << 13
};
ENUM_FLAG_OPERATORS(EBufferUsageFlags)

enum EResourceState : uint32_t
{
	Unknown = UINT32_MAX,
	Common = 0x0,
	VertexBuffer = 0x1,
	UniformBuffer = 0x2,
	IndexBuffer = 0x4,
	RenderTarget = 0x8,
	UnorderedAccess = 0x10,
	DepthWrite = 0x20,
	DepthRead = 0x40,
	StreamOut = 0x80,
	IndirectArgument = 0x100,
	TransferDst = 0x200,
	TransferSrc = 0x400,
	ResolveDst = 0x800,
	ResolveSrc = 0x1000,
	AccelerationStructure = 0x2000,
	ShadingRate = 0x4000,
	ShaderResource = 0x8000,
	Present = 0x10000,
	InputAttachment = 0x20000
};

struct IHashedResourceDesc
{
	virtual size_t Hash() const = 0;
protected:
	static constexpr size_t InvalidHash = ~0ull;
	mutable size_t HashValue = InvalidHash;
};

template<class T>
class IHWObject
{
public:
	using Type = std::remove_extent_t<T>;

	IHWObject() = default;

	explicit IHWObject(Type* Handle)
		: m_Handle(Handle)
	{
	}

	IHWObject(const IHWObject& Other)
		: m_Handle(Other.m_Handle)
	{
	}

	IHWObject(IHWObject&& Other)
		: m_Handle(Other.m_Handle)
	{
		std::exchange(other.m_Handle, {});
	}

	IHWObject& operator=(const IHWObject& Other)
	{
		if (m_Handle != Other.m_Handle)
		{
			m_Handle = Other.m_Handle;
		}
		return *this;
	}

	IHWObject& operator=(IHWObject&& Other)
	{
		if (m_Handle != Other.m_Handle)
		{
			m_Handle = std::exchange(Other.m_Handle, {});
		}
		return *this;
	}

	bool8_t operator()() const
	{
		return IsValid();
	}

	virtual ~IHWObject()
	{
		std::exchange(m_Handle, {});
	}

	inline T** Reference()
	{
		return &m_Handle;
	}

	inline const T** Reference() const
	{
		return &m_Handle;
	}

	inline T* Get()
	{
		assert(m_Handle);
		return m_Handle;
	}

	inline const T* const Get() const
	{
		assert(m_Handle);
		return m_Handle;
	}

	inline T* operator->()
	{
		assert(m_Handle);
		return m_Handle;
	}

	inline T* operator->() const
	{
		assert(m_Handle);
		return m_Handle;
	}

	inline const bool8_t IsValid() const
	{
		return m_Handle != nullptr;
	}

	inline uint64_t ToUInt64() const
	{
		return reinterpret_cast<uint64_t>(m_Handle);
	}
protected:
	T* m_Handle = nullptr;
};

struct IHWResource
{
public:
	IHWResource() = default;
	virtual ~IHWResource() = default;

	IHWResource(const char8_t* Name)
		: m_DebugName(Name)
	{
	}

	void SetDebugName(const char8_t* Name) { m_DebugName = Name; }
	
	const char8_t* GetDebugName() const { return m_DebugName.c_str(); }
protected:
private:
	std::string m_DebugName;
};

template<class T, class AnyObject>
inline T* Cast(AnyObject* Object)
{
	return static_cast<T*>(Object);
}

NAMESPACE_END(RHI)
