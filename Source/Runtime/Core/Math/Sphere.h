#pragma once

#include "Runtime/Core/Math/Vector4.h"

NAMESPACE_START(Gear)
NAMESPACE_START(Math)

class Sphere
{
public:
protected:
private:
	Vector3 m_Center;
	float32_t m_Radius;
};

NAMESPACE_END(Math)
NAMESPACE_END(Gear)
